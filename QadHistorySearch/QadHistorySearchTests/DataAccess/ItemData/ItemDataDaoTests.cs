﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using QadHistorySearch.DataAccess.ItemData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.DataAccess.ItemData.Tests
{
    [TestClass()]
    public class ItemDataDaoTests
    {
        [TestMethod()]
        public void ItemGroupTest()
        {
            Assert.IsTrue(ItemDataDao.Instance.ItemGroup("2414421-1830Q5F")=="FINF");
            Assert.IsTrue(ItemDataDao.Instance.ItemGroup("2414421-1880Q5U") == "FACF");
        }
    }
}