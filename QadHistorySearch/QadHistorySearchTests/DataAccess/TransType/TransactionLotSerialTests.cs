﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using QadHistorySearch.DataAccess.TransType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.DataAccess.TransType.Tests
{
    [TestClass()]
    public class TransactionLotSerialTests
    {
        [TestMethod()]
        public void PieceWithoutClaimSymbolTest()
        {
            Assert.IsNotNull(TransactionLotSerial.PieceWithoutClaimSymbol("Z16808989R"));
        }

        [TestMethod()]
        public void IsReturnedPieceTest()
        {
            Assert.IsTrue(TransactionLotSerial.IsReturnedPiece("Z16808989R"));
        }

        [TestMethod()]
        public void PieceWithoutClaimSymbolReducedTest()
        {
            Assert.IsTrue(TransactionLotSerial.PieceWithoutClaimSymbolReduced("Z17120700260301R") == "Z171207002603");
        }
    }
}