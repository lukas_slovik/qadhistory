﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using QadHistorySearch.DataAccess.Bom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.DataAccess.Bom.Tests
{
    [TestClass()]
    public class BomDaoTests
    {
        [TestMethod()]
        public void GetParentItemTest()
        {
            Assert.IsTrue(BomDao.Instance.GetParentItem("2414421-1830Q5F")== "2414421-1880Q5U");
        }
    }
}