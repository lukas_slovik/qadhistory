﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using QadHistorySearch.DataAccess;
using QadHistorySearch.DataAccess.Bom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.DataAccess.Tests
{
    [TestClass()]
    public class TransHistoryDaoTests
    {
        [TestMethod()]
        public void NewTransactionsTest()
        {
            List<TR_HIST> foundData = TransHistoryDao.Instance.NewTransactions();
            Assert.IsNotNull(foundData);
        }

        [TestMethod()]
        public void FindPreviousTransactionTest()
        {
            TR_HIST olderTransaction = new TR_HIST();
            olderTransaction.TR_TRNBR = 1127632;
            olderTransaction.TR_PART = "2414421-1830Q5F";
            olderTransaction.TR_SERIAL = "Z170227005002";
            olderTransaction.TR_REF = "";
            TR_HIST foundData = TransHistoryDao.Instance.FindPreviousTransaction(olderTransaction);
            Assert.IsNotNull(foundData);
        }

        [TestMethod()]
        public void FinParentItemTransactionTest()
        {
            TR_HIST olderTransaction = new TR_HIST();
            olderTransaction.TR_TRNBR = 1127826;
            olderTransaction.TR_PART = "2515915-z195009f";
            olderTransaction.TR_SERIAL = "Z1703030007";
            olderTransaction.TR_REF = "";
            olderTransaction.TR_LOT = "521863";
            List<TR_HIST> foundData = TransHistoryDao.Instance.FindParentItemTransaction(BomDao.Instance.GetParentItem(olderTransaction.TR_PART), olderTransaction);
            Assert.IsTrue(foundData.Count > 0);
        }

        [TestMethod()]
        public void FindIssueUnplannedReceiveTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SaleOrderTransactionTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ConsumedQtyTest()
        {
            Assert.Fail();
        }
    }
}