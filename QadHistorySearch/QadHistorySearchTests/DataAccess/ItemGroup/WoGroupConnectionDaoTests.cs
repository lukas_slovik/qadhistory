﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using QadHistorySearch.DataAccess.ItemGroup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.DataAccess.ItemGroup.Tests
{
    [TestClass()]
    public class WoGroupConnectionDaoTests
    {
        [TestMethod()]
        public void DataTest()
        {
            Assert.IsTrue(WoGroupConnectionDao.Instance.Data().Count > 0);
        }

        [TestMethod()]
        public void GetIssueGroupTest()
        {
            Assert.IsTrue(WoGroupConnectionDao.Instance.GetIssueGroup("FACF")=="BEAM");
        }

        [TestMethod()]
        public void GetReciveGroupTest()
        {
            Assert.IsTrue(WoGroupConnectionDao.Instance.GetReciveGroup("FACF") == "FINF");
        }
    }
}