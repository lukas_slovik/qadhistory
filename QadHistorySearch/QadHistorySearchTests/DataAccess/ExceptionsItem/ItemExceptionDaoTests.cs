﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using QadHistorySearch.DataAccess.ExceptionsItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.DataAccess.ExceptionsItem.Tests
{
    [TestClass()]
    public class ItemExceptionDaoTests
    {
        [TestMethod()]
        public void ItemExceludedFromHistorySearchTest()
        {
            Assert.IsFalse(ItemExceptionDao.Instance.ItemExceludedFromHistorySearch(null));
            Assert.IsFalse(ItemExceptionDao.Instance.ItemExceludedFromHistorySearch(""));
            Assert.IsFalse(ItemExceptionDao.Instance.ItemExceludedFromHistorySearch("2306940-184052F"));
            Assert.IsTrue(ItemExceptionDao.Instance.ItemExceludedFromHistorySearch("2306718-VSUBPTO"));
        }
    }
}