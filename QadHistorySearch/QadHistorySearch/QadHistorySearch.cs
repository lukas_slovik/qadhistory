﻿using QadHistorySearch.ServiceOperation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch
{
    public partial class QadHistorySearch : ServiceBase
    {
        private ServiceOperations operations;
        public QadHistorySearch()
        {
            this.operations = new ServiceOperations();
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            this.operations.StartServiceOperations();
        }

        protected override void OnStop()
        {
            this.operations.StopServiceOperations();
        }
    }
}
