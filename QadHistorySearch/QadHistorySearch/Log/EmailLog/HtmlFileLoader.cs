﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.Log.EmailLog
{
    public class HtmlFileLoader
    {
        private static string FilePath(string fileName)
        {
            if (fileName != null)
            {
                return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + Properties.Settings.Default.HtmlEmailTemplateLocation + fileName;
            }
            return null;
        }

        private static bool FileExists(string filePath)
        {
            if (filePath != null)
                return File.Exists(filePath);
            return false;
        }

        public static string GetHtmlData(string fileName)
        {
            if (FileExists(FilePath(fileName)))
            {
                try
                {
                    return File.ReadAllText(FilePath(fileName));
                }
                catch (Exception e)
                {
                    EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                }
            }
            return null;
        }
    }
}
