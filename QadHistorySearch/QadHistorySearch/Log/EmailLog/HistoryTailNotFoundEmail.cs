﻿using QadHistorySearch.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.Log.EmailLog
{
    public class HistoryTailNotFoundEmail : EmailSend
    {
        private Dictionary<string, string> PrepareData(Guid newId, TR_HIST transaction)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            if (transaction != null)
            {
                data.Add("logo", newId.ToString());
                data.Add("transactionNbr", transaction.TR_TRNBR.ToString());
                data.Add("itemNumber", transaction.TR_PART.ToString());
                data.Add("lotSerial", transaction.TR_SERIAL.ToString());
                data.Add("reference", transaction.TR_REF.ToString());
            }
            return data;
        }

        public HistoryTailNotFoundEmail(TR_HIST transaction)
        {
            Guid newId = Guid.NewGuid();
            SetHtmlView(TemplateFiller.Fill(HtmlFileLoader.GetHtmlData(Properties.Settings.Default.EmailTemplateHistorySearchIssueFileName), PrepareData(newId, transaction)));
            AddRecipients(Properties.Settings.Default.HistorySearchIssueRecepients);
            AddInlineResource(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) +
                Properties.Settings.Default.HtmlEmailTemplateLocation +
                Properties.Settings.Default.EmailTemplateCompanyLogo,
                newId.ToString());
            Subject = Properties.Settings.Default.EmailTemplateHistorySearchIssueSubject;
        }
    }
}
