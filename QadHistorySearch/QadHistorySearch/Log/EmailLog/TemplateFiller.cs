﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.Log.EmailLog
{
    public class TemplateFiller
    {
        private static string TemlateKey(string key)
        {
            if (key != null)
                return Properties.Settings.Default.EmailTemplateIdStart + key + Properties.Settings.Default.EmailTemplateIdEnd;
            return "";
        }

        public static string Fill(string template, Dictionary<string, string> data)
        {
            string filledTemplate = "";
            if (template != null && data != null)
            {
                filledTemplate = template;
                foreach (var pair in data)
                {
                    filledTemplate = filledTemplate.Replace(TemlateKey(pair.Key), pair.Value);
                }
            }
            return filledTemplate;
        }
    }
}
