﻿using QadHistorySearch.WinCulture;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QadHistorySearch.Log.EmailLog
{
    public class EmailSend
    {
        private MailMessage emailMsg;

        public EmailSend()
        {
            this.emailMsg = new MailMessage();
            this.emailMsg.From = new MailAddress(Properties.Settings.Default.DefaultMailName + "@" + Properties.Settings.Default.MailDomain, Properties.Settings.Default.DefaultMailName, System.Text.Encoding.UTF8);
        }

        public void AddRecipient(string emailAddr)
        {
            this.emailMsg.To.Add(new MailAddress(emailAddr, ""));
        }

        public void AddRecipients(string emailAddrs)
        {
            string[] recepients = emailAddrs.Split(Properties.Settings.Default.EmailRecepientsSplitChar);
            foreach (string recepient in recepients)
            {
                this.emailMsg.To.Add(new MailAddress(recepient, ""));
            }
        }
        public void AddRecipient(string emailAddr, string displayName)
        {
            this.emailMsg.To.Add(new MailAddress(emailAddr, displayName));
        }

        public string Subject
        {
            set
            {
                this.emailMsg.Subject = value;
            }
        }

        public void AddAttachment(string fileName)
        {
            if (File.Exists(fileName))
            {
                CultureType orgCulture = new CultureType(); ;
                orgCulture.OriginalCulture = Thread.CurrentThread.CurrentCulture;
                orgCulture.SetBarcodeCulture();
                Attachment attachment = new Attachment(fileName, MediaTypeNames.Application.Pdf);
                ContentDisposition disposition = attachment.ContentDisposition;
                disposition.CreationDate = File.GetCreationTime(fileName);
                disposition.ModificationDate = File.GetLastWriteTime(fileName);
                disposition.ReadDate = File.GetLastAccessTime(fileName);
                disposition.FileName = Path.GetFileName(fileName);
                disposition.Size = new FileInfo(fileName).Length;
                disposition.DispositionType = DispositionTypeNames.Attachment;
                this.emailMsg.Attachments.Add(attachment);
                orgCulture.SetOriginalCulture();
            }
        }

        public string AddInlineResource(string fileName, string contentId)
        {
            if (File.Exists(fileName))
            {
                CultureType orgCulture = new CultureType(); ;
                orgCulture.OriginalCulture = Thread.CurrentThread.CurrentCulture;
                orgCulture.SetBarcodeCulture();
                Attachment attachment = new Attachment(fileName);
                ContentDisposition disposition = attachment.ContentDisposition;
                disposition.CreationDate = File.GetCreationTime(fileName);
                disposition.ModificationDate = File.GetLastWriteTime(fileName);
                disposition.ReadDate = File.GetLastAccessTime(fileName);
                disposition.FileName = Path.GetFileName(fileName);
                disposition.Size = new FileInfo(fileName).Length;
                disposition.Inline = true;
                attachment.ContentId = contentId;
                disposition.DispositionType = DispositionTypeNames.Inline;
                this.emailMsg.Attachments.Add(attachment);
                orgCulture.SetOriginalCulture();
                return attachment.ContentId;
            }
            return null;
        }

        public void SendEmail()
        {
            SmtpClient client = new SmtpClient(Properties.Settings.Default.SmtpServerIp);
            try
            {
                client.Send(this.emailMsg);
            }
            catch (Exception e)
            {
                EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
            }
        }

        public void SetHtmlView(string htmlString)
        {
            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(htmlString, null, MediaTypeNames.Text.Html);
            this.emailMsg.AlternateViews.Add(avHtml);
        }

        public void SetHtmlViewFromFile(string htmlFileName)
        {
            string htmlView = HtmlFileLoader.GetHtmlData(htmlFileName);
            if (htmlView != null)
            {
                AlternateView avHtml = AlternateView.CreateAlternateViewFromString(htmlView, null, MediaTypeNames.Text.Html);
                this.emailMsg.AlternateViews.Add(avHtml);
            }
            else
            {
                AlternateView avHtml = AlternateView.CreateAlternateViewFromString("", null, MediaTypeNames.Text.Html);
                this.emailMsg.AlternateViews.Add(avHtml);
            }
        }

        public void SetBody(string bodyText)
        {
            this.emailMsg.Body = bodyText;
        }
    }
}
