﻿using QadHistorySearch.DataAccess;
using QadHistorySearch.DataAccess.ExceptionsItem;
using QadHistorySearch.DataAccess.SetupParameters;
using QadHistorySearch.History;
using QadHistorySearch.Log.EmailLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QadHistorySearch.ServiceOperation
{
    public class HistorySearchOperation : OperationCore
    {
        public HistorySearchOperation() : base(false, Properties.Settings.Default.HistSearchTransCheckRefreshInMs)
        {
        }

        public override void Start()
        {
            Thread operations = new Thread(RepeatedOperation);
            operations.Start();
        }

        public override void Stop()
        {
            ShouldStop = true;
        }

        protected override void RunOperation()
        {
            List<TR_HIST> newTransactions = TransHistoryDao.Instance.NewTransactions();
            foreach (TR_HIST transaction in newTransactions)
            {
                if (!ItemExceptionDao.Instance.ItemExceludedFromHistorySearch(transaction.TR_PART))
                {
                    FindHistory history = new FindHistory(transaction);
                    if (!history.FindAndUpdateHistory())
                    {
                        HistoryTailNotFoundEmail issueEmail = new HistoryTailNotFoundEmail(transaction);
                        issueEmail.SendEmail();
                    }
                }
                Parameters.Standard.Values.LastTrTrnbrHistSearch = transaction.TR_TRNBR;
                Parameters.Standard.SaveChanges();
            }
        }
    }
}
