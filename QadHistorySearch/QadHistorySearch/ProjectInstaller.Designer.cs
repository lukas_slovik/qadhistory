﻿namespace QadHistorySearch
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.QadHistorySearchProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.QadHistorySearchInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // QadHistorySearchProcessInstaller
            // 
            this.QadHistorySearchProcessInstaller.Password = null;
            this.QadHistorySearchProcessInstaller.Username = null;
            // 
            // QadHistorySearchInstaller
            // 
            this.QadHistorySearchInstaller.DelayedAutoStart = true;
            this.QadHistorySearchInstaller.Description = "QadHistorySearch";
            this.QadHistorySearchInstaller.DisplayName = "QadHistorySearch";
            this.QadHistorySearchInstaller.ServiceName = "QadHistorySearch";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.QadHistorySearchProcessInstaller,
            this.QadHistorySearchInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller QadHistorySearchProcessInstaller;
        private System.ServiceProcess.ServiceInstaller QadHistorySearchInstaller;
    }
}