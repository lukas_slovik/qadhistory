﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QadHistorySearch.WinCulture
{
    public class CultureType
    {
        private CultureInfo current = CultureInfo.CurrentCulture;
        public CultureInfo OriginalCulture
        {
            set { this.current = value; }
        }
        public void SetBarcodeCulture()
        {
            CultureInfo cI = new CultureInfo("en-US", true);
            Thread.CurrentThread.CurrentCulture = cI;
        }
        public void SetShipperCulture()
        {
            CultureInfo cI = new CultureInfo("sk-sk", true);
            Thread.CurrentThread.CurrentCulture = cI;
        }
        public void SetOriginalCulture()
        {
            Thread.CurrentThread.CurrentCulture = this.current;
        }
    }
}
