﻿USE [MESERP]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WoGroupConnection](
	[ReceiveGroup] [nvarchar](8) NOT NULL,
	[MainIssueGroup] [nvarchar](8) NOT NULL,
 CONSTRAINT [PK_WoGroupConnection] PRIMARY KEY CLUSTERED 
(
	[ReceiveGroup] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[LotHistoryTail](
	[ItemNumber] [nvarchar](20) NOT NULL,
	[LotSerial] [nvarchar](20) NOT NULL,
	[Reference] [nvarchar](50) NOT NULL,
	[AddedTime] [datetime] NOT NULL,
	[WoNbr] [nvarchar](18) NOT NULL,
	[WoId] [nvarchar](8) NOT NULL,
	[ReceiveGroup] [nvarchar](8) NOT NULL,
	[Reported] [bit] NULL,
 CONSTRAINT [PK_LotHistoryTail] PRIMARY KEY CLUSTERED 
(
	[ItemNumber] ASC,
	[LotSerial] ASC,
	[Reference] ASC,
	[WoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LotHistoryTail]  WITH CHECK ADD  CONSTRAINT [fk_LotHistoryTailWoGroupConnection] FOREIGN KEY([ReceiveGroup])
REFERENCES [dbo].[WoGroupConnection] ([ReceiveGroup])
GO

ALTER TABLE [dbo].[LotHistoryTail] CHECK CONSTRAINT [fk_LotHistoryTailWoGroupConnection]
GO

CREATE TABLE [dbo].[VendorLotHistoryTail](
	[WoNbr] [nvarchar](18) NOT NULL,
	[WoId] [nvarchar](8) NOT NULL,
	[VendLotItemNumber] [nvarchar](45) NOT NULL,
	[VendLot] [nvarchar](45) NOT NULL,
 CONSTRAINT [PK_VendorLotHistoryTail] PRIMARY KEY CLUSTERED 
(
	[WoNbr] ASC,
	[WoId] ASC,
	[VendLot] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
