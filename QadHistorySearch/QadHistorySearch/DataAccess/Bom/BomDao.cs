﻿using QadHistorySearch.DataAccess.ItemData;
using QadHistorySearch.DataAccess.ItemGroup;
using QadHistorySearch.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.DataAccess.Bom
{
    public class BomDao : TimeRelevant
    {
        private static BomDao instance;

        public static BomDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new BomDao();
                return instance;
            }
        }

        protected override int TimeOutPeriod
        {
            get
            {
                return Properties.Settings.Default.BomRefreshTime;
            }
        }

        protected override bool NotInitialized
        {
            get
            {
                return (this.bomData == null);
            }
        }

        private List<BOM> bomData;
        private object bomDataLock;
        private BomDao()
        {
            this.bomDataLock = new object();
            this.bomData = null;
        }

        public List<BOM> AllData
        {
            get
            {
                if (ShouldRefresh)
                {
                    using (var Context = new MFGPRO_SQLEntities())
                    {
                        var requestQuery = from bom in Context.BOMs
                                           select bom;
                        try
                        {
                            lock (this.bomDataLock)
                            {
                                this.bomData = requestQuery.ToList<BOM>();
                            }
                            Updated(DateTime.Now);
                        }
                        catch (Exception e)
                        {
                            EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                        }
                    }
                }
                lock (this.bomDataLock)
                {
                    if (this.bomData != null)
                        return new List<BOM>(this.bomData);
                }
                return new List<BOM>();
            }
        }

        public string GetParentItem(string childItem)
        {
            if (childItem != null)
            {
                string group = ItemDataDao.Instance.ItemGroup(childItem);
                if (group != null)
                {
                    string issueGroup = WoGroupConnectionDao.Instance.GetIssueGroup(group);
                    if (issueGroup != null)
                    {
                        BOM partExists = AllData.Find(bom => bom.Type.Trim().ToUpper() == group &&
                        bom.ItemNumber.Trim().ToUpper() == childItem.Trim().ToUpper() &&
                            bom.CompType.Trim().ToUpper() == issueGroup);
                        if (partExists != null)
                            return partExists.CompNumber.ToUpper().Trim();
                    }
                }
            }
            return null;
        }
    }
}
