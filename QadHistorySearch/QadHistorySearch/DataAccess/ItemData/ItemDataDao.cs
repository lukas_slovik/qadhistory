﻿using QadHistorySearch.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.DataAccess.ItemData
{
    public class ItemDataDao : TimeRelevant
    {
        private static ItemDataDao instance;

        public static ItemDataDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new ItemDataDao();
                return instance;
            }
        }

        protected override bool NotInitialized
        {
            get
            {
                return this.itemData == null;
            }
        }

        protected override int TimeOutPeriod
        {
            get
            {
                return Properties.Settings.Default.ItemMasterRefreshTime;
            }
        }

        private List<Matricula> itemData;
        private object itemDataLock;
        private ItemDataDao()
        {
            this.itemDataLock = new object();
            this.itemData = null;
        }

        public List<Matricula> All
        {
            get
            {
                if (ShouldRefresh)
                {
                    using (var Context = new MFGPRO_SQLEntities())
                    {
                        var requestQuery = from item in Context.Matriculas
                                           select item;
                        try
                        {
                            lock (this.itemDataLock)
                            {
                                this.itemData = requestQuery.ToList<Matricula>();
                            }
                            Updated(DateTime.Now);
                        }
                        catch (Exception e)
                        {
                            EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                        }
                    }
                }
                lock (this.itemDataLock)
                {
                    if (this.itemData != null)
                        return new List<Matricula>(this.itemData);
                }
                return new List<Matricula>();
            }
        }

        public List<Matricula> NonObsoleteItems
        {
            get
            {
                return All.FindAll(item => item.PT_STATUS != Properties.Settings.Default.ItemObsoleteStatus);
            }
        }

        public string ItemGroup(string itemNumber)
        {
            if (itemNumber != null)
            {
                Matricula itemExists = All.Find(item => item.Matricula1.Trim().ToUpper() == itemNumber.Trim().ToUpper());
                if (itemExists != null)
                    return itemExists.TipoArticulo.Trim().ToUpper();
            }
            return null;
        }
    }
}
