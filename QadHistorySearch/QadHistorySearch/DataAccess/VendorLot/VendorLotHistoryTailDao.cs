﻿using QadHistorySearch.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.DataAccess.VendorLot
{
    public class VendorLotHistoryTailDao
    {
        private static VendorLotHistoryTailDao instance;

        public static VendorLotHistoryTailDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new VendorLotHistoryTailDao();
                return instance;
            }
        }

        private VendorLotHistoryTailDao()
        {

        }

        public bool AddVendorLot(LotHistoryTail tail,TR_HIST vendorLotData)
        {
            if(tail!=null && vendorLotData != null)
            {
                using (var Context = new MESERPEntities())
                {
                    try
                    {
                        VendorLotHistoryTail newTail = new VendorLotHistoryTail();
                        newTail.WoNbr = tail.WoNbr;
                        newTail.WoId = tail.WoId;
                        newTail.VendLotItemNumber = vendorLotData.TR_PART.Trim().ToUpper();
                        newTail.VendLot = vendorLotData.TR_SERIAL.Trim().ToUpper();
                        Context.VendorLotHistoryTails.Add(newTail);
                        Context.SaveChanges();
                        return true;
                    }
                    catch (Exception e)
                    {
                        EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                    }
                }
            }
            return false;
        }
    }
}
