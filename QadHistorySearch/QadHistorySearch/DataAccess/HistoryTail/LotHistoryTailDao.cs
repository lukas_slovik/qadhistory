﻿using QadHistorySearch.DataAccess.ItemData;
using QadHistorySearch.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.DataAccess.HistoryTail
{
    public class LotHistoryTailDao
    {
        private static LotHistoryTailDao instance;

        public static LotHistoryTailDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new LotHistoryTailDao();
                return instance;
            }
        }

        private LotHistoryTailDao()
        {

        }

        private List<LotHistoryTail> PieceHistory(string itemNumber, string lotSerial, string reference)
        {
            using (var Context = new MESERPEntities())
            {
                var requestQuery = from hist in Context.LotHistoryTails
                                   where hist.ItemNumber.Trim().ToUpper() == itemNumber.ToUpper().Trim() &&
                                   hist.LotSerial.Trim().ToUpper() == lotSerial.ToUpper().Trim() &&
                                   hist.Reference.Trim().ToUpper() == reference.ToUpper().Trim()
                                   select hist;
                try
                {
                    return requestQuery.ToList<LotHistoryTail>();
                }
                catch (Exception e)
                {
                    EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                }
            }
            return new List<LotHistoryTail>();
        }

        public List<LotHistoryTail> GetPieceHistory(string itemNumber, string lotSerial)
        {
            if (itemNumber != null && lotSerial != null)
                return PieceHistory(itemNumber, lotSerial, "");
            return new List<LotHistoryTail>();
        }

        public List<LotHistoryTail> GetPieceHistory(string itemNumber, string lotSerial, string reference)
        {
            if (itemNumber != null && lotSerial != null && reference != null)
                return PieceHistory(itemNumber, lotSerial, reference);
            return new List<LotHistoryTail>();
        }

        public bool CopyHistoryTail(List<LotHistoryTail> foundTail, TR_HIST transaction)
        {
            if (foundTail != null && transaction != null)
            {
                using (var Context = new MESERPEntities())
                {
                    try
                    {
                        DateTime addedTime = DateTime.Now;
                        foreach (LotHistoryTail tail in foundTail)
                        {
                            LotHistoryTail newTail = new LotHistoryTail();
                            newTail.ItemNumber = transaction.TR_PART.Trim().ToUpper();
                            newTail.LotSerial = transaction.TR_SERIAL.Trim().ToUpper();
                            newTail.Reference = transaction.TR_REF.Trim().ToUpper();
                            newTail.AddedTime = addedTime;
                            newTail.WoNbr = tail.WoNbr;
                            newTail.WoId = tail.WoId;
                            newTail.ReceiveGroup = tail.ReceiveGroup;
                            newTail.Reported = null;
                            Context.LotHistoryTails.Add(newTail);
                        }
                        Context.SaveChanges();
                        return true;
                    }
                    catch (Exception e)
                    {
                        EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                    }
                }
            }
            return false;
        }

        public bool AddHistoryTail(TR_HIST transaction, TR_HIST tailTransaction)
        {
            if (tailTransaction != null && transaction != null)
            {
                using (var Context = new MESERPEntities())
                {
                    try
                    {
                        LotHistoryTail newTail = new LotHistoryTail();
                        newTail.ItemNumber = transaction.TR_PART.Trim().ToUpper();
                        newTail.LotSerial = transaction.TR_SERIAL.Trim().ToUpper();
                        newTail.Reference = transaction.TR_REF.Trim().ToUpper();
                        newTail.AddedTime = DateTime.Now;
                        newTail.WoNbr = tailTransaction.TR_NBR;
                        newTail.WoId = tailTransaction.TR_LOT;
                        newTail.ReceiveGroup = ItemDataDao.Instance.ItemGroup(tailTransaction.TR_PART);
                        newTail.Reported = null;
                        Context.LotHistoryTails.Add(newTail);
                        Context.SaveChanges();
                        return true;
                    }
                    catch (Exception e)
                    {
                        EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                    }
                }
            }
            return false;
        }
    }
}
