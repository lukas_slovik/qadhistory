﻿using QadHistorySearch.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.DataAccess.SetupParameters
{
    public class Parameters
    {
        private static Parameters standard;

        public static Parameters Standard
        {
            get
            {
                if (standard == null)
                    standard = new Parameters();
                return standard;
            }
        }

        private ParamValue values;
        private object valuesLock;

        private Parameters()
        {
            this.values = null;
            this.valuesLock = new object();
        }

        protected bool ShouldRefresh
        {
            get
            {
                return (this.values == null);
            }
        }

        private object ParamVal(SetupParameter param)
        {
            if (param.ParameterValueText != null)
                return param.ParameterValueText;
            if (param.ParameteValueNumber != null)
                return param.ParameteValueNumber;
            if (param.ParameterValueBit != null)
                return param.ParameterValueBit;
            if (param.ParameterValueDateTime != null)
                return param.ParameterValueDateTime;
            return null;
        }

        private void SetParamVal(SetupParameter param, object value)
        {
            if (value != null)
            {
                if (param.ParameterValueText != null)
                    param.ParameterValueText = value.ToString();
                if (param.ParameteValueNumber != null)
                {
                    double convertRes = 0;
                    if (Double.TryParse(value.ToString(), out convertRes))
                        param.ParameteValueNumber = convertRes;
                }

                if (param.ParameterValueBit != null)
                {
                    bool result = false;
                    if (Boolean.TryParse(value.ToString(), out result))
                        param.ParameterValueBit = result;
                }
                if (param.ParameterValueDateTime != null)
                {
                    DateTime result = DateTime.Now;
                    if (DateTime.TryParse(value.ToString(), out result))
                        param.ParameterValueDateTime = result;
                }
            }
        }

        private List<string> Names()
        {
            List<string> paramNames = new List<string>();
            foreach (PropertyInfo property in this.values.GetType().GetProperties())
            {
                paramNames.Add(property.Name);
            }
            return paramNames;
        }

        public ParamValue Values
        {
            get
            {
                if (ShouldRefresh)
                {
                    lock (this.valuesLock)
                    {
                        this.values = new ParamValue();
                        List<SetupParameter> allParameters = SetupParameterDao.Instance.Parameters(Names());
                        foreach (SetupParameter param in allParameters)
                        {
                            PropertyInfo prop = this.values.GetType().GetProperty(param.ParameterName);
                            if (prop != null)
                            {
                                if (TypeCompare.IsNumeric(ParamVal(param)) && TypeCompare.IsNumeric(prop.PropertyType))
                                    prop.SetValue(this.values, Convert.ChangeType(ParamVal(param), prop.PropertyType), null);
                                else if (ParamVal(param).GetType() == prop.PropertyType)
                                    prop.SetValue(this.values, Convert.ChangeType(ParamVal(param), prop.PropertyType), null);
                            }
                        }
                    }
                }
                lock (this.valuesLock)
                {
                    return this.values;
                }
            }
        }

        public void SaveChanges()
        {
            lock (this.valuesLock)
            {
                if (this.values != null)
                {
                    List<SetupParameter> allParameters = SetupParameterDao.Instance.Parameters(Names());
                    foreach (SetupParameter param in allParameters)
                    {
                        PropertyInfo prop = this.values.GetType().GetProperty(param.ParameterName);
                        if (prop != null)
                        {
                            EventLogMultiThread.AddDebugLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), "Parameter to update : " + param.ParameterName + " actual: " + param.ParameteValueNumber);
                            SetParamVal(param, prop.GetValue(this.values));
                            EventLogMultiThread.AddDebugLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), "Parameter to update : " + param.ParameterName + " new: " + param.ParameteValueNumber);
                        }
                    }
                    SetupParameterDao.Instance.UpdateParameters(allParameters);
                }
            }
        }
    }
}
