﻿using QadHistorySearch.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.DataAccess.SetupParameters
{
    public class SetupParameterDao : TimeRelevant
    {
        private static SetupParameterDao instance;

        public static SetupParameterDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new SetupParameterDao();
                return instance;
            }
        }

        protected override int TimeOutPeriod
        {
            get
            {
                return Properties.Settings.Default.SetupParameterRefreshTime;
            }
        }

        protected override bool NotInitialized
        {
            get
            {
                return (this.parameters == null);
            }
        }

        private List<SetupParameter> parameters;
        private object parametersLock;
        private SetupParameterDao()
        {
            this.parameters = null;
            this.parametersLock = new object();
        }

        public List<SetupParameter> Parameters(List<string> paramNames)
        {
            if (ShouldRefresh)
            {
                string[] parameterNames = paramNames.ToArray();
                using (var Context = new MESERPEntities())
                {
                    var requestQuery = from parameter in Context.SetupParameters
                                       where parameterNames.Contains(parameter.ParameterName)
                                       select parameter;
                    try
                    {
                        lock (this.parametersLock)
                        {
                            this.parameters = requestQuery.ToList<SetupParameter>();
                        }
                        Updated(DateTime.Now);
                    }
                    catch (Exception e)
                    {
                        EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                    }
                }
            }
            lock (this.parametersLock)
            {
                if (this.parameters != null)
                    return new List<SetupParameter>(this.parameters);
            }
            return new List<SetupParameter>();
        }

        public void UpdateParameters(List<SetupParameter> updatedParams)
        {
            if (updatedParams != null)
            {
                List<string> paramNames = new List<string>();
                foreach (SetupParameter setParam in updatedParams)
                {
                    paramNames.Add(setParam.ParameterName);
                }
                string[] parameterNames = paramNames.ToArray();
                lock (this.parametersLock)
                {
                    using (var Context = new MESERPEntities())
                    {
                        var requestQuery = from parameter in Context.SetupParameters
                                           where parameterNames.Contains(parameter.ParameterName)
                                           select parameter;
                        try
                        {
                            List<SetupParameter> existingParameters = requestQuery.ToList<SetupParameter>();
                            if (existingParameters != null)
                            {
                                foreach (SetupParameter param in existingParameters)
                                {
                                    SetupParameter foundParam = updatedParams.Find(par => par.ParameterName == param.ParameterName);
                                    if (foundParam != null)
                                    {

                                        if (param.ParameterValueText != foundParam.ParameterValueText)
                                            param.ParameterValueText = foundParam.ParameterValueText;
                                        if (param.ParameteValueNumber != foundParam.ParameteValueNumber)
                                        {
                                            EventLogMultiThread.AddDebugLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), "value changed acutal : " + param.ParameteValueNumber + " new: " + foundParam.ParameteValueNumber);
                                            param.ParameteValueNumber = foundParam.ParameteValueNumber;
                                            EventLogMultiThread.AddDebugLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), "value changed to : " + param.ParameteValueNumber);

                                        }
                                        if (param.ParameterValueBit != foundParam.ParameterValueBit)
                                            param.ParameterValueBit = foundParam.ParameterValueBit;
                                        if (param.ParameterValueDateTime != foundParam.ParameterValueDateTime)
                                            param.ParameterValueDateTime = foundParam.ParameterValueDateTime;
                                    }
                                }
                                int rowsUpdated = Context.SaveChanges();
                                if (rowsUpdated > 0)
                                    EventLogMultiThread.AddDebugLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), "Changes saved : " + rowsUpdated);

                                this.parameters = existingParameters;
                                Updated(DateTime.Now);
                            }
                        }
                        catch (Exception e)
                        {
                            EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                        }
                    }
                }
            }
        }
    }
}
