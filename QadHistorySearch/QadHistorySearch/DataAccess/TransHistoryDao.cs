﻿using QadHistorySearch.DataAccess.ItemData;
using QadHistorySearch.DataAccess.SetupParameters;
using QadHistorySearch.DataAccess.TransType;
using QadHistorySearch.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.DataAccess
{
    public class TransHistoryDao
    {
        private static TransHistoryDao instance;

        public static TransHistoryDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new TransHistoryDao();
                return instance;
            }
        }

        private TransHistoryDao()
        {

        }

        public List<TR_HIST> NewTransactions()
        {
            using (var Context = new MFGPRO_SQLEntities())
            {
                var requestQuery = from transaction in Context.TR_HIST
                                   where transaction.TR_TRNBR > Parameters.Standard.Values.LastTrTrnbrHistSearch &&
                                   transaction.TR_RMKS.Trim() != Properties.Settings.Default.EndSplitValueName &&
                                   TransactionType.DownloadTransactions.Contains(transaction.TR_TYPE) && transaction.TR_QTY_LOC > 0
                                   select transaction;
                try
                {
                    return requestQuery.Take<TR_HIST>(Properties.Settings.Default.MaxTransactionsOnOneLoad).ToList();
                }
                catch (Exception e)
                {
                    EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                }
            }
            return new List<TR_HIST>();
        }

        public TR_HIST FindIssueTransation(string transactionType, string trLot)
        {
            if (transactionType != null && trLot != null)
            {
                using (var Context = new MFGPRO_SQLEntities())
                {
                    var requestQuery = from transaction in Context.TR_HIST
                                       where transaction.TR_TYPE.Trim().ToUpper() == transactionType.Trim().ToUpper() &&
                                       transaction.TR_LOT == trLot
                                       select transaction;
                    try
                    {
                        return requestQuery.FirstOrDefault<TR_HIST>();
                    }
                    catch (Exception e)
                    {
                        EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                    }
                }
            }
            return null;
        }

        public TR_HIST FindPreviousTransaction(TR_HIST previousTransaction)
        {
            if (previousTransaction != null)
            {
                using (var Context = new MFGPRO_SQLEntities())
                {
                    var requestQuery = from transaction in Context.TR_HIST
                                       where transaction.TR_TRNBR < previousTransaction.TR_TRNBR &&
                                       transaction.TR_PART == previousTransaction.TR_PART &&
                                       transaction.TR_SERIAL == previousTransaction.TR_SERIAL &&
                                       transaction.TR_REF == previousTransaction.TR_REF &&
                                       transaction.TR_RMKS.Trim() != Properties.Settings.Default.EndSplitValueName &&
                                       TransactionType.DownloadTransactions.Contains(transaction.TR_TYPE)
                                       orderby transaction.TR_TRNBR descending
                                       select transaction;

                    try
                    {
                        return requestQuery.FirstOrDefault<TR_HIST>();
                    }
                    catch (Exception e)
                    {
                        EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                    }
                }
            }
            return null;
        }

        public List<TR_HIST> FindParentItemTransaction(string parentItemNumber, TR_HIST previousTransaction)
        {
            if (parentItemNumber != null && previousTransaction != null)
            {
                if (ItemDataDao.Instance.ItemGroup(previousTransaction.TR_PART) == Properties.Settings.Default.TopLevelReceiveGroup)
                {
                    using (var Context = new MFGPRO_SQLEntities())
                    {
                        var requestQuery = from transaction in Context.TR_HIST
                                           where transaction.TR_TRNBR < previousTransaction.TR_TRNBR &&
                                           transaction.TR_PART == parentItemNumber &&
                                           transaction.TR_NBR == previousTransaction.TR_NBR &&
                                           transaction.TR_RMKS.Trim() != Properties.Settings.Default.EndSplitValueName &&
                                           transaction.TR_QTY_LOC < 0 &&
                                           transaction.TR_TYPE.Trim().ToUpper() == Properties.Settings.Default.WorkOrderIssueTransactionTypes
                                           orderby transaction.TR_TRNBR descending
                                           select transaction;

                        try
                        {
                            return requestQuery.ToList<TR_HIST>();
                        }
                        catch (Exception e)
                        {
                            EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                        }
                    }
                }
                else
                {
                    using (var Context = new MFGPRO_SQLEntities())
                    {
                        var requestQuery = from transaction in Context.TR_HIST
                                           where transaction.TR_TRNBR < previousTransaction.TR_TRNBR &&
                                           transaction.TR_PART == parentItemNumber &&
                                           transaction.TR_LOT == previousTransaction.TR_LOT &&
                                           transaction.TR_RMKS.Trim() != Properties.Settings.Default.EndSplitValueName &&
                                           transaction.TR_QTY_LOC < 0 &&
                                           transaction.TR_TYPE.Trim().ToUpper() == Properties.Settings.Default.WorkOrderIssueTransactionTypes
                                           orderby transaction.TR_TRNBR descending
                                           select transaction;

                        try
                        {
                            return requestQuery.ToList<TR_HIST>();
                        }
                        catch (Exception e)
                        {
                            EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                        }
                    }
                }
            }
            return new List<TR_HIST>();
        }

        public TR_HIST FindIssueUnplannedReceive(TR_HIST previousTransaction)
        {
            if (previousTransaction != null)
            {
                using (var Context = new MFGPRO_SQLEntities())
                {
                    var requestQuery = from transaction in Context.TR_HIST
                                       where transaction.TR_TRNBR < previousTransaction.TR_TRNBR &&
                                       transaction.TR_SERIAL == previousTransaction.TR_SERIAL &&
                                       transaction.TR_REF == previousTransaction.TR_REF &&
                                       transaction.TR_LOC == previousTransaction.TR_LOC &&
                                       transaction.TR_QTY_LOC < 0
                                       orderby transaction.TR_TRNBR descending
                                       select transaction;

                    try
                    {
                        return requestQuery.FirstOrDefault<TR_HIST>();
                    }
                    catch (Exception e)
                    {
                        EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                    }
                }
            }
            return null;
        }

        public TR_HIST SaleOrderTransaction(TR_HIST previousTransaction)
        {
            if (previousTransaction != null)
            {
                string claimLotSerial = TransactionLotSerial.PieceWithoutClaimSymbol(previousTransaction.TR_SERIAL);
                string claimLotSerialRed = TransactionLotSerial.PieceWithoutClaimSymbolReduced(previousTransaction.TR_SERIAL);
                using (var Context = new MFGPRO_SQLEntities())
                {
                    var requestQuery = from transaction in Context.TR_HIST
                                       where transaction.TR_TRNBR < previousTransaction.TR_TRNBR &&
                                       (transaction.TR_SERIAL == claimLotSerial ||
                                       transaction.TR_SERIAL == claimLotSerialRed) &&
                                       transaction.TR_REF == previousTransaction.TR_REF &&
                                       transaction.TR_TYPE == Properties.Settings.Default.SalesOrderTransactionTypes &&
                                       transaction.TR_QTY_LOC < 0
                                       orderby transaction.TR_TRNBR descending
                                       select transaction;

                    try
                    {
                        return requestQuery.FirstOrDefault<TR_HIST>();
                    }
                    catch (Exception e)
                    {
                        EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                    }
                }
            }
            return null;
        }

        public double ConsumedQty(string woId, string lotSerial, string itemNumber, string reference)
        {
            if (woId != null && lotSerial != null && itemNumber != null && reference != null)
            {
                using (var Context = new MFGPRO_SQLEntities())
                {
                    var requestQuery = from transaction in Context.TR_HIST
                                       where transaction.TR_SERIAL == lotSerial &&
                                       transaction.TR_PART == itemNumber &&
                                       transaction.TR_REF == reference &&
                                       transaction.TR_LOT == woId &&
                                       transaction.TR_TYPE == Properties.Settings.Default.WorkOrderIssueTransactionTypes
                                       select transaction;

                    try
                    {
                        List<TR_HIST> consumedQtyTransactions = requestQuery.ToList<TR_HIST>();
                        double consumedQty = 0;
                        foreach (TR_HIST transaction in consumedQtyTransactions)
                        {
                            if (transaction.TR_QTY_LOC != null)
                                consumedQty += (double)transaction.TR_QTY_LOC;
                        }
                        return consumedQty;
                    }
                    catch (Exception e)
                    {
                        EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                    }
                }
            }
            return 0;
        }
    }
}
