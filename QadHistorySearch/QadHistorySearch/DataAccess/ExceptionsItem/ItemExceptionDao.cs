﻿using QadHistorySearch.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.DataAccess.ExceptionsItem
{
    public class ItemExceptionDao : TimeRelevant
    {
        private static ItemExceptionDao instance;

        public static ItemExceptionDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new ItemExceptionDao();
                return instance;
            }
        }

        protected override int TimeOutPeriod
        {
            get
            {
                return Properties.Settings.Default.ItemExceptionRefreshTime;
            }
        }

        protected override bool NotInitialized
        {
            get
            {
                lock (this.allItemExcetionsLock)
                {
                    return (this.allItemExcetions == null);
                }
            }
        }

        private List<ItemException> allItemExcetions;
        private object allItemExcetionsLock;
        private ItemExceptionDao()
        {
            this.allItemExcetionsLock = new object();
        }

        public List<ItemException> Exceptions
        {
            get
            {
                if (ShouldRefresh)
                {
                    using (var Context = new MESERPEntities())
                    {
                        var requestQuery = from exception in Context.ItemExceptions
                                           select exception;
                        try
                        {
                            lock (this.allItemExcetionsLock)
                            {
                                this.allItemExcetions = requestQuery.ToList<ItemException>();
                            }
                            Updated(DateTime.Now);
                        }
                        catch (Exception e)
                        {
                            EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                        }
                    }
                }
                lock (this.allItemExcetionsLock)
                {
                    if (this.allItemExcetions != null)
                        return new List<ItemException>(this.allItemExcetions);
                }
                return new List<ItemException>();
            }
        }

        public bool ItemExceludedFromHistorySearch(string itemNumber)
        {
            if (itemNumber != null)
            {
                return (Exceptions.Find(exception => exception.ItemNumber.Trim().ToUpper() == itemNumber.Trim().ToUpper()
                 && exception.IdItemExceptionType == Properties.Settings.Default.IdItemExceptionExcludeFromHistoryTailSearch) != null);
            }
            return false;
        }
    }
}
