﻿using QadHistorySearch.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.DataAccess.ItemGroup
{
    public class WoGroupConnectionDao : TimeRelevant
    {
        private static WoGroupConnectionDao instance;

        public static WoGroupConnectionDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new WoGroupConnectionDao();
                return instance;
            }
        }

        protected override int TimeOutPeriod
        {
            get
            {
                return Properties.Settings.Default.WoGroupConnectionRefreshTime;
            }
        }

        protected override bool NotInitialized
        {
            get
            {
                return (this.allData == null);
            }
        }

        private List<WoGroupConnection> allData;
        private object allDataLock;
        private WoGroupConnectionDao()
        {
            this.allData = null;
            this.allDataLock = new object();
        }

        public List<WoGroupConnection> Data()
        {
            if (ShouldRefresh)
            {
                using (var Context = new MESERPEntities())
                {
                    var requestQuery = from parameter in Context.WoGroupConnections
                                       select parameter;
                    try
                    {
                        lock (this.allDataLock)
                        {
                            this.allData = requestQuery.ToList<WoGroupConnection>();
                        }
                        Updated(DateTime.Now);
                    }
                    catch (Exception e)
                    {
                        EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                    }
                }
            }
            lock (this.allDataLock)
            {
                if (this.allData != null)
                    return new List<WoGroupConnection>(this.allData);
            }
            return new List<WoGroupConnection>();
        }

        public string GetIssueGroup(string receiveGroup)
        {
            if (receiveGroup != null)
            {
                List<WoGroupConnection> allGroupData = Data();
                WoGroupConnection foundGroup = allGroupData.Find(connection => connection.ReceiveGroup.Trim().ToUpper() == receiveGroup.Trim().ToUpper());
                if (foundGroup != null)
                    return foundGroup.MainIssueGroup.Trim().ToUpper();
            }
            return null;
        }

        public string GetReciveGroup(string issueGroup)
        {
            if (issueGroup != null)
            {
                List<WoGroupConnection> allGroupData = Data();
                WoGroupConnection foundGroup = allGroupData.Find(connection => connection.MainIssueGroup.Trim().ToUpper() == issueGroup.Trim().ToUpper());
                if (foundGroup != null)
                    return foundGroup.ReceiveGroup.Trim().ToUpper();
            }
            return null;
        }
    }
}
