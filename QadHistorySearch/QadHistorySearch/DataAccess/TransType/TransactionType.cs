﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.DataAccess.TransType
{
    public class TransactionType
    {
        public static string[] DownloadTransactions
        {
            get
            {
                return Properties.Settings.Default.DownloadTransactionTypes.Split(Properties.Settings.Default.SettingsListSeparator);
            }
        }

        public static bool IsMovementReceive(string transactionType)
        {
            if (transactionType != null)
            {
                string[] allowedTransactions = Properties.Settings.Default.MovementReceiveTransactionTypes.Split(Properties.Settings.Default.SettingsListSeparator);
                return allowedTransactions.Contains(transactionType.Trim().ToUpper());
            }
            return false;
        }

        public static bool IsMovementIssue(string transactionType)
        {
            if (transactionType != null)
            {
                string[] allowedTransactions = Properties.Settings.Default.MovementIssueTransactionTypes.Split(Properties.Settings.Default.SettingsListSeparator);
                return allowedTransactions.Contains(transactionType.Trim().ToUpper());
            }
            return false;
        }

        public static bool IsWorkOrderReceive(string transactionType)
        {
            if (transactionType != null)
            {
                string[] allowedTransactions = Properties.Settings.Default.WorkOrderReceiveTransactionTypes.Split(Properties.Settings.Default.SettingsListSeparator);
                return allowedTransactions.Contains(transactionType.Trim().ToUpper());
            }
            return false;
        }

        public static bool IsWorkOrderIssue(string transactionType)
        {
            if (transactionType != null)
            {
                string[] allowedTransactions = Properties.Settings.Default.WorkOrderIssueTransactionTypes.Split(Properties.Settings.Default.SettingsListSeparator);
                return allowedTransactions.Contains(transactionType.Trim().ToUpper());
            }
            return false;
        }

        public static bool IsPurchaseReceive(string transactionType)
        {
            if (transactionType != null)
            {
                string[] allowedTransactions = Properties.Settings.Default.PurchaseReceiveTransactionTypes.Split(Properties.Settings.Default.SettingsListSeparator);
                return allowedTransactions.Contains(transactionType.Trim().ToUpper());
            }
            return false;
        }

        public static bool IsUnplanned(string transactionType)
        {
            if (transactionType != null)
            {
                string[] allowedTransactions = Properties.Settings.Default.UnplannedTransactionTypes.Split(Properties.Settings.Default.SettingsListSeparator);
                return allowedTransactions.Contains(transactionType.Trim().ToUpper());
            }
            return false;
        }

        public static bool IsSalesOrder(string transactionType)
        {
            if (transactionType != null)
            {
                string[] allowedTransactions = Properties.Settings.Default.SalesOrderTransactionTypes.Split(Properties.Settings.Default.SettingsListSeparator);
                return allowedTransactions.Contains(transactionType.Trim().ToUpper());
            }
            return false;
        }
    }
}
