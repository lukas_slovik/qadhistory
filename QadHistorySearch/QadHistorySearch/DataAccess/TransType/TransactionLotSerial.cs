﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.DataAccess.TransType
{
    public class TransactionLotSerial
    {
        public static bool IsReturnedPiece(string lotSerial)
        {
            if (lotSerial != null)
                return lotSerial.ToUpper().Trim().Contains(Properties.Settings.Default.CustomerClaimMaterialSymbol);
            return false;
        }

        public static string PieceWithoutClaimSymbol(string lotSerial)
        {
            if (lotSerial != null)
                if (lotSerial.Trim().ToUpper().IndexOf(Properties.Settings.Default.CustomerClaimMaterialSymbol) > -1)
                    return lotSerial.Trim().ToUpper().Substring(0, lotSerial.Trim().ToUpper().IndexOf(Properties.Settings.Default.CustomerClaimMaterialSymbol));
            return "";
        }

        public static string PieceWithoutClaimSymbolReduced(string lotSerial)
        {
            if (lotSerial != null)
            {
                if (lotSerial.Trim().ToUpper().IndexOf(Properties.Settings.Default.CustomerClaimMaterialSymbol) > -1)
                {
                    string withoutClaim = lotSerial.Trim().ToUpper().Substring(0, lotSerial.Trim().ToUpper().IndexOf(Properties.Settings.Default.CustomerClaimMaterialSymbol));
                    if (withoutClaim.Length > 2)
                        return withoutClaim.Substring(0, withoutClaim.Length - 2);
                }
            }
            return "";
        }
    }
}
