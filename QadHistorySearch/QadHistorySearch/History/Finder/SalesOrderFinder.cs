﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QadHistorySearch.DataAccess;
using QadHistorySearch.DataAccess.TransType;

namespace QadHistorySearch.History.Finder
{
    public class SalesOrderFinder : TransactionFinder
    {
        private TR_HIST transaction;
        public SalesOrderFinder(TR_HIST transaction)
        {
            this.transaction = transaction;
        }

        public TR_HIST FoundTransaction()
        {
            if (transaction != null)
            {
                if (transaction.TR_QTY_LOC > 0)
                {
                    if (TransactionLotSerial.IsReturnedPiece(transaction.TR_SERIAL))
                        return TransHistoryDao.Instance.SaleOrderTransaction(transaction);
                    else
                        return TransHistoryDao.Instance.FindPreviousTransaction(transaction);
                }
                else
                {
                    return TransHistoryDao.Instance.FindPreviousTransaction(transaction);
                }
            }
            return null;
        }
    }
}
