﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QadHistorySearch.DataAccess;

namespace QadHistorySearch.History.Finder
{
    public class UnplannedFinder : TransactionFinder
    {
        private TR_HIST transaction;
        public UnplannedFinder(TR_HIST transaction)
        {
            this.transaction = transaction;
        }

        public TR_HIST FoundTransaction()
        {
            if (this.transaction != null)
            {
                if (this.transaction.TR_QTY_LOC > 0)
                {
                    TR_HIST fromPiece = TransHistoryDao.Instance.FindIssueUnplannedReceive(this.transaction);
                    if (fromPiece != null)
                        return fromPiece;
                    // save history tale
                }
            }
            return null;
        }
    }
}
