﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QadHistorySearch.DataAccess;

namespace QadHistorySearch.History.Finder
{
    public class IssueMovementFinder : TransactionFinder
    {
        private TR_HIST transaction;
        public IssueMovementFinder(TR_HIST transaction)
        {
            this.transaction = transaction;
        }

        public TR_HIST FoundTransaction()
        {
            if (Properties.Settings.Default.ReciveTransactionType == this.transaction.TR_TYPE)
                return TransHistoryDao.Instance.FindIssueTransation(Properties.Settings.Default.IssueTransactionType, this.transaction.TR_LOT);
            else if (Properties.Settings.Default.ReceiveStatusTransactionType == this.transaction.TR_TYPE)
                return TransHistoryDao.Instance.FindIssueTransation(Properties.Settings.Default.IssueStatusTransactionType, this.transaction.TR_LOT);
            return null;
        }
    }
}
