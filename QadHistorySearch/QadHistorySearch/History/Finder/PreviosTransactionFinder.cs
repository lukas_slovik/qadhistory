﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QadHistorySearch.DataAccess;

namespace QadHistorySearch.History.Finder
{
    public class PreviosTransactionFinder : TransactionFinder
    {
        private TR_HIST transaction;
        public PreviosTransactionFinder(TR_HIST transaction)
        {
            this.transaction = transaction;
        }

        public TR_HIST FoundTransaction()
        {
            return TransHistoryDao.Instance.FindPreviousTransaction(transaction);
        }
    }
}
