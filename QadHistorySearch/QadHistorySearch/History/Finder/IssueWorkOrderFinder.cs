﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QadHistorySearch.DataAccess;
using QadHistorySearch.DataAccess.Bom;
using QadHistorySearch.Log;

namespace QadHistorySearch.History.Finder
{
    public class IssueWorkOrderFinder : TransactionFinder
    {
        private TR_HIST transaction;
        public IssueWorkOrderFinder(TR_HIST transaction)
        {
            this.transaction = transaction;
        }

        public TR_HIST FoundTransaction()
        {
            if (this.transaction != null)
            {
                List<TR_HIST> issueTransactions = TransHistoryDao.Instance.FindParentItemTransaction(BomDao.Instance.GetParentItem(this.transaction.TR_PART), this.transaction);
                var groupedByLotSerial = issueTransactions.GroupBy(trans => trans.TR_SERIAL.ToUpper().Trim());
                if (groupedByLotSerial.Count() > 0)
                {
                    if (groupedByLotSerial.Count() > 1)
                    {
                        List<TR_HIST> consumedPieceTransactions = new List<TR_HIST>();
                        foreach (TR_HIST transaction in issueTransactions)
                        {
                            if (TransHistoryDao.Instance.ConsumedQty(transaction.TR_LOT, transaction.TR_SERIAL, transaction.TR_PART, transaction.TR_REF) != 0)
                                consumedPieceTransactions.Add(transaction);
                        }
                        var consumedPieces = consumedPieceTransactions.GroupBy(trans => trans.TR_SERIAL.ToUpper().Trim());
                        if (consumedPieces.Count() > 1)
                        {
                            EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), "Error in consumption more than one piece used as main parent item " + this.transaction.TR_TRNBR);
                        }
                        if (consumedPieceTransactions.Count > 0)
                            return consumedPieceTransactions[0];
                    }
                    else
                    {
                        return issueTransactions[0];
                    }
                }
            }
            return null;
        }
    }
}
