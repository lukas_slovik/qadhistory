﻿using QadHistorySearch.DataAccess;
using QadHistorySearch.DataAccess.HistoryTail;
using QadHistorySearch.DataAccess.VendorLot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.History
{
    public class HistoryTailSaver
    {
        private TR_HIST transaction;
        public HistoryTailSaver(TR_HIST transaction)
        {
            this.transaction = transaction;
        }

        private bool IsOriginalLot(TR_HIST newTransaction)
        {
            return this.transaction.TR_PART.Trim().ToUpper() == newTransaction.TR_PART.Trim().ToUpper() &&
                this.transaction.TR_SERIAL.Trim().ToUpper() == newTransaction.TR_SERIAL.Trim().ToUpper() &&
                this.transaction.TR_REF.Trim().ToUpper() == newTransaction.TR_REF.Trim().ToUpper();
        }

        public bool NewTransactionFound(TR_HIST newTransaction)
        {
            if (newTransaction != null)
            {
                List<LotHistoryTail> historyTail = LotHistoryTailDao.Instance.GetPieceHistory(newTransaction.TR_PART, newTransaction.TR_SERIAL, newTransaction.TR_REF);
                if (historyTail.Count > 0)
                {
                    if (IsOriginalLot(newTransaction))
                        return true;
                    return LotHistoryTailDao.Instance.CopyHistoryTail(historyTail, this.transaction);
                }
                else
                {
                    if (newTransaction.TR_TYPE.Trim().ToUpper() == Properties.Settings.Default.WorkOrderReceiveTransactionTypes)
                        LotHistoryTailDao.Instance.AddHistoryTail(this.transaction, newTransaction);
                    else if (newTransaction.TR_TYPE.Trim().ToUpper() == Properties.Settings.Default.PurchaseReceiveTransactionTypes ||
                        newTransaction.TR_TYPE.Trim().ToUpper() == Properties.Settings.Default.UnplannedTransactionTypes)
                    {
                        LotHistoryTail tailVendorGroup = LotHistoryTailDao.Instance.GetPieceHistory(transaction.TR_PART, transaction.TR_SERIAL, transaction.TR_REF).Find(tail => tail.ReceiveGroup == Properties.Settings.Default.TopLevelReceiveGroup);
                        VendorLotHistoryTailDao.Instance.AddVendorLot(tailVendorGroup, newTransaction);
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
