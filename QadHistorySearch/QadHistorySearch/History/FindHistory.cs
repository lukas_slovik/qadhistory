﻿using QadHistorySearch.DataAccess;
using QadHistorySearch.DataAccess.TransType;
using QadHistorySearch.History.Finder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadHistorySearch.History
{
    public class FindHistory
    {
        private TR_HIST transaction;
        public FindHistory(TR_HIST transaction)
        {
            if (transaction == null)
                throw new ArgumentNullException();
            this.transaction = transaction;
        }

        public bool FindAndUpdateHistory()
        {
            TR_HIST transactionMovement = this.transaction;
            HistoryTailSaver history = new HistoryTailSaver(this.transaction);
            while (transactionMovement != null)
            {
                if (history.NewTransactionFound(transactionMovement))
                    return true;
                TransactionFinder finder = null;
                if (TransactionType.IsMovementReceive(transactionMovement.TR_TYPE))
                {
                    finder = new IssueMovementFinder(transactionMovement);
                    // find movent issue transaction;
                }
                else if (TransactionType.IsMovementIssue(transactionMovement.TR_TYPE))
                {
                    //find RCT-WO,RCT-TR,RCT-CHL,RCT-PO,ISS-SO,ISS-UNP TR_TRNBR<transaction.TR_TRNBR
                    finder = new PreviosTransactionFinder(transactionMovement);
                }
                else if (TransactionType.IsWorkOrderReceive(transactionMovement.TR_TYPE))
                {
                    //find ISS-WO transaction TR_TRNBR<transaction.TR_TRNBR and main part
                    finder = new IssueWorkOrderFinder(transactionMovement);
                }
                else if (TransactionType.IsWorkOrderIssue(transactionMovement.TR_TYPE))
                {
                    // find RCT-WO,RCT-TR,RCT-CHL,RCT-PO,ISS-SO,ISS-UNP TR_TRNBR<transaction.TR_TRNBR
                    finder = new PreviosTransactionFinder(transactionMovement);
                }
                else if (TransactionType.IsPurchaseReceive(transactionMovement.TR_TYPE))
                {
                    // if qty>0 finish search
                    finder = new PurchaseReceiveFinder(transactionMovement);
                }
                else if (TransactionType.IsUnplanned(transactionMovement.TR_TYPE))
                {
                    // if ISS-UNP qty>0 and no ISS-UNP for same lotserial reference location qty<0
                    finder = new UnplannedFinder(transactionMovement);
                }
                else if (TransactionType.IsSalesOrder(transactionMovement.TR_TYPE))
                {
                    // if ISS-UNP qty>0 and no ISS-UNP for same lotserial reference location qty<0
                    finder = new SalesOrderFinder(transactionMovement);
                }
                if (finder != null)
                    transactionMovement = finder.FoundTransaction();
                else
                    return false;
                }
            return false;
        }
    }
}
